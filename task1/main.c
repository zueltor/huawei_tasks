#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

#define FILE_IN "in.txt"
#define FILE_OUT "out.txt"
#define EPS 1e-3

//#define PRINT_MATRIX

/**
 * Matrix is stored in one dimensional array. Two dimensional array is for convenience
 */
struct Matrix_t {
    int rows_count;
    int columns_count;
    double *one_dimensional_array;
    double **array;
};

typedef struct Matrix_t Matrix;

Matrix *initialiseMatrix(int m, int n) {
    Matrix *matr = (Matrix *) malloc(sizeof(Matrix));
    matr->rows_count = m;
    matr->columns_count = n;
    matr->one_dimensional_array = (double *) malloc(n * m * sizeof(double));
    matr->array = (double **) malloc(m * sizeof(double *));
    for (int i = 0; i < m; i++) {
        matr->array[i] = &matr->one_dimensional_array[i * n];
        for (int j = 0; j < n; j++) {
            matr->array[i][j] = 0;
        }
    }
    return matr;
}

void freeArray(Matrix *matrix) {
    free(matrix->array);
    free(matrix->one_dimensional_array);
}

void freeMatrix(Matrix *matrix) {
    freeArray(matrix);
    free(matrix);
}

int readMatrix(FILE *in, Matrix *matrix) {
    fscanf(in, "%d %d\n", &matrix->rows_count, &matrix->columns_count);
    if (matrix->rows_count < 1 || matrix->columns_count < 1) {
        fprintf(stderr, "Incorrect matrix size\n");
        return EXIT_FAILURE;
    }
    freeArray(matrix);
    matrix->one_dimensional_array = (double *) malloc(matrix->columns_count * matrix->rows_count * sizeof(double));
    matrix->array = (double **) malloc(matrix->rows_count * sizeof(double *));
    for (int i = 0; i < matrix->rows_count; i++) {
        matrix->array[i] = &matrix->one_dimensional_array[i * matrix->columns_count];
        for (int j = 0; j < matrix->columns_count; j++) {
            fscanf(in, "%lf", &matrix->array[i][j]);
        }
    }
    return EXIT_SUCCESS;
}


void sendSize(int rank, int _rows, int _columns) {
    MPI_Send(&_rows, 1, MPI_INT, rank, 0, MPI_COMM_WORLD);
    MPI_Send(&_columns, 1, MPI_INT, rank, 0, MPI_COMM_WORLD);
}

void sendMatrixElements(Matrix *matrix, int rank) {
    MPI_Send(matrix->one_dimensional_array, matrix->columns_count * matrix->rows_count, MPI_DOUBLE, rank, 0,
             MPI_COMM_WORLD);
}

void sendMatrix(Matrix *matrix, int rank) {
    sendSize(rank, matrix->rows_count, matrix->columns_count);
    sendMatrixElements(matrix, rank);
}

/**
 * Return approximate number of rows that every process will have
 */
int getChunkSize(int rows_count, int size) {
    return (rows_count + size - 1) / size;
}

/**
 * Return first index of row (inclusive)
 */
int getChunkStart(int rows_count, int chunk, int count_chunks) {
    int chunk_size = getChunkSize(rows_count, count_chunks);
    int start = chunk * chunk_size;
    if (start > rows_count) {
        return rows_count;
    } else {
        return start;
    }
}

/**
 * Return last index of row (not inclusive)
 */
int getChunkEnd(int rows_count, int chunk, int count_chunks) {
    int chunk_size = getChunkSize(rows_count, count_chunks);
    int end = (chunk + 1) * chunk_size;
    if (end > rows_count) {
        return rows_count;
    } else {
        return end;
    }
}

/**
 * Return number of rows that will have the process with rank chunk
 */
int getRowsToSend(int rows_count, int chunk, int count_chunks) {
    return getChunkEnd(rows_count, chunk, count_chunks) - getChunkStart(rows_count, chunk, count_chunks);
}

/**
 * Send part of matrix to process with this rank
 */
void sendSubmatrix(Matrix *matrix, int rank, int size) {
    int start_index = getChunkStart(matrix->rows_count, rank, size);
    int end_index = getChunkEnd(matrix->rows_count, rank, size);
    int rows_to_send = end_index - start_index;
    sendSize(rank, rows_to_send, matrix->columns_count);
    MPI_Send(matrix->array[start_index], matrix->columns_count * rows_to_send, MPI_DOUBLE, rank, 0, MPI_COMM_WORLD);
}

void receiveMatrix(Matrix *matrix) {
    MPI_Recv(&matrix->rows_count, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    MPI_Recv(&matrix->columns_count, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    freeArray(matrix);
    matrix->one_dimensional_array = (double *) malloc(matrix->columns_count * matrix->rows_count * sizeof(double));
    matrix->array = (double **) malloc(matrix->rows_count * sizeof(double *));
    MPI_Recv(matrix->one_dimensional_array, matrix->columns_count * matrix->rows_count, MPI_DOUBLE, MPI_ANY_SOURCE, 0,
             MPI_COMM_WORLD,
             MPI_STATUS_IGNORE);
    for (int i = 0; i < matrix->rows_count; i++) {
        matrix->array[i] = &matrix->one_dimensional_array[i * matrix->columns_count];
    }
}

void printMatrix(Matrix *matrix) {
    for (int i = 0; i < matrix->rows_count; i++) {
        for (int j = 0; j < matrix->columns_count; j++) {
            printf("%lf\t", matrix->array[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void copySubmatrix(Matrix *matrix_dst, Matrix *matrix_src, int size) {
    matrix_dst->rows_count = getChunkSize(matrix_src->rows_count, size);
    matrix_dst->columns_count = matrix_src->columns_count;
    freeArray(matrix_dst);
    matrix_dst->one_dimensional_array = (double *) malloc(
            matrix_dst->columns_count * matrix_dst->rows_count * sizeof(double));
    matrix_dst->array = (double **) malloc(matrix_dst->rows_count * sizeof(double *));
    for (int i = 0; i < matrix_dst->rows_count; i++) {
        matrix_dst->array[i] = &matrix_dst->one_dimensional_array[i * matrix_dst->columns_count];
        for (int j = 0; j < matrix_dst->columns_count; j++) {
            matrix_dst->array[i][j] = matrix_src->array[i][j];
        }
    }
}

Matrix *multiply(Matrix *matr, Matrix *b) {
    Matrix *c = initialiseMatrix(matr->rows_count, b->columns_count);
    for (int i = 0; i < matr->rows_count; i++) {
        for (int j = 0; j < matr->columns_count; j++) {
            for (int k = 0; k < b->columns_count; k++) {
                c->array[i][k] += matr->array[i][j] * b->array[j][k];
            }
        }
    }
    return c;
}

void gatherMatrix(Matrix *matr, Matrix *c, int size) {
    int end = getRowsToSend(matr->rows_count, 0, size) * matr->columns_count;
    for (int i = 0; i < end; i++) {
        matr->one_dimensional_array[i] = c->one_dimensional_array[i];
    }
    int next_row_index = getRowsToSend(matr->rows_count, 0, size);
    for (int rank = 1; rank < size; rank++) {
        int to_receive = getRowsToSend(matr->rows_count, rank, size);
        MPI_Recv(matr->array[next_row_index], to_receive * matr->columns_count, MPI_DOUBLE, rank, 0, MPI_COMM_WORLD,
                 MPI_STATUS_IGNORE);
        next_row_index += to_receive;
    }
}

int openFileAndReadMatrices(Matrix *matrix_A, Matrix *matrix_B) {
    int error;
    FILE*in = fopen(FILE_IN, "r");
    if (in == NULL) {
        perror("Failed to open input file");
        return EXIT_FAILURE;
    }
    error = readMatrix(in, matrix_A);
    if (error) {
        fclose(in);
        return EXIT_FAILURE;
    }
#ifdef PRINT_MATRIX
    printf("Matrix A:\n");
    printMatrix(matrix_A);
#endif
    error = readMatrix(in, matrix_B);
    if (error) {
        fclose(in);
        return EXIT_FAILURE;
    }
#ifdef PRINT_MATRIX
    printf("Matrix B:\n");
    printMatrix(matrix_B);
#endif
    if (matrix_A->columns_count != matrix_B->rows_count) {
        fprintf(stderr,
                "Cannot multiply matrices: Matrix A columns count should be equal to Matrix B rows count\n");
        fclose(in);
        return EXIT_FAILURE;
    }
    fclose(in);
    return EXIT_SUCCESS;
}

/**
 * Compare matrices with precision EPS
 */
void compareResult(Matrix *matrix) {
    int columns_count, rows_count;
    FILE*out = fopen(FILE_OUT, "r");
    if (out == NULL) {
        perror("Could not open output file");
    }
    fscanf(out, "%d %d", &rows_count, &columns_count);
    if (rows_count != matrix->rows_count || columns_count != matrix->columns_count) {
        printf("Incorrect result: matrix sizes are not equal\n");
    }
    int count = matrix->columns_count * matrix->rows_count;
    double value;
    double *a = matrix->one_dimensional_array;
    for (int i = 0; i < count; i++) {
        fscanf(out, "%lf", &value);
        if (fabs(value - a[i]) > EPS) {
            printf("Incorrect result: %lf and %lf\n", value, a[i]);
            fclose(out);
            return;
        }
    }
    fclose(out);
    printf("Correct result\n");
}

int main(int argc, char **argv) {
    int size, rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    Matrix *matrix_A = initialiseMatrix(0, 0);
    Matrix *matrix_B = initialiseMatrix(0, 0);
    Matrix *submatrix_A = initialiseMatrix(0, 0);
    Matrix *submatrix_C = NULL;
    int error;
    if (rank == 0) {
        error = openFileAndReadMatrices(matrix_A, matrix_B);
    }

    //Broadcast value of error, exit program if value != 0
    MPI_Bcast(&error, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (error) {
        MPI_Finalize();
        freeMatrix(matrix_A);
        freeMatrix(matrix_B);
        freeMatrix(submatrix_A);
        return EXIT_FAILURE;
    }

    double start_time = 0, finish_time = 0;

    if (rank == 0) {
        printf("Starting multiplication\n");
        start_time = MPI_Wtime();
        for (int sendTo = 1; sendTo < size; sendTo++) {
            //Send part of matrix A to process with rank sendTo
            sendSubmatrix(matrix_A, sendTo, size);
            //Send matrix B to process with rank sendTo
            sendMatrix(matrix_B, sendTo);
        }
        //Copy part of matrix A into submatrix_A
        copySubmatrix(submatrix_A, matrix_A, size);
    } else {
        //Receive matrices sent by rank 0
        receiveMatrix(submatrix_A);
        receiveMatrix(matrix_B);
    }

    //Multiplication, every process multiplies different submatrix A by matrix B
    submatrix_C = multiply(submatrix_A, matrix_B);

    if (rank == 0) {
        Matrix *matrix_C = initialiseMatrix(matrix_A->rows_count, matrix_B->columns_count);
        //Receive all submatrices from all processes into matrix C
        gatherMatrix(matrix_C, submatrix_C, size);
#ifdef PRINT_MATRIX
        printf("Matrix C:\n");
        printMatrix(matrix_C);
#endif
        printf("Finished multiplication\n");
        finish_time = MPI_Wtime();
        printf("Time taken: %lf sec\n", finish_time - start_time);

        printf("Comparing result of multiplication with values in %s...\n", FILE_OUT);
        compareResult(matrix_C);
        freeMatrix(matrix_C);
    } else {
        sendMatrixElements(submatrix_C, 0);
    }
    freeMatrix(matrix_A);
    freeMatrix(matrix_B);
    freeMatrix(submatrix_A);
    freeMatrix(submatrix_C);
    MPI_Finalize();
    return EXIT_SUCCESS;
}
