#include <iostream>
#include <ctime>
#include <fstream>
#include <cstdlib>

class Matrix {
public:
    double **a;
    int rows_count;
    int columns_count;
public:
    explicit Matrix(int m, int n) : rows_count(m), columns_count(n), a(new double *[m]) {
        for (int i = 0; i < rows_count; i++) {
            a[i] = new double[columns_count];
            for (int j = 0; j < columns_count; j++) {
                a[i][j] = 0;
            }
        }
    }

    void generate() {
        for (int i = 0; i < rows_count; i++) {
            for (int j = 0; j < columns_count; j++) {
                a[i][j] = (double) rand() / RAND_MAX * 300 - 150;
            }
        }
    }

    Matrix(const Matrix &m) {
        rows_count = m.rows_count;
        columns_count = m.columns_count;
        a = new double *[rows_count];
        for (int i = 0; i < rows_count; i++) {
            a[i] = new double[columns_count];
            for (int j = 0; j < columns_count; j++) {
                a[i][j] = m.a[i][j];
            }
        }
    }

    Matrix &operator=(const Matrix &m) {
        if (this != &m) {
            for (int i = 0; i < rows_count; i++) {
                delete[] a[i];
            }
            delete[] a;
            rows_count = m.rows_count;
            columns_count = m.columns_count;
            a = new double *[rows_count];
            for (int i = 0; i < rows_count; i++) {
                a[i] = new double[columns_count];
                for (int j = 0; j < columns_count; j++) {
                    a[i][j] = m.a[i][j];
                }
            }
        }
        return *this;
    }

    void fprint(const std::string &filename, std::ios_base::openmode mode = std::ios_base::out) const {
        std::ofstream out(filename, mode);
        out.precision(15);
        out << rows_count << " " << columns_count << std::endl;
        for (int i = 0; i < rows_count; i++) {
            for (int j = 0; j < columns_count; j++) {
                out << a[i][j] << " ";
            }
            out << std::endl;
        }
    }

    Matrix operator*(const Matrix &m) {
        Matrix res(rows_count, m.columns_count);
        for (int i = 0; i < rows_count; i++) {
            for (int j = 0; j < columns_count; j++) {
                for (int k = 0; k < m.columns_count; k++) {
                    res.a[i][k] += a[i][j] * m.a[j][k];
                }
            }
        }
        return res;
    }

    ~Matrix() {
        for (int i = 0; i < rows_count; i++) {
            delete[] a[i];
        }
        delete[] a;
    }
};

int main(int argc, char **argv) {
    srand(time(nullptr));
    int m = (argc >= 2) ? atoi(argv[1]) : 5;
    int n = (argc >= 3) ? atoi(argv[2]) : 4;
    int k = (argc >= 4) ? atoi(argv[3]) : 2;
    std::string in = (argc >= 5) ? argv[4] : "in.txt";
    std::string out = (argc == 6) ? argv[5] : "out.txt";
    printf("Program arguments: m, n, k, input file, output file\n"
           "Example: ./a.out 5 4 2 in.txt out.txt\n");
    printf("M = %d, N = %d, K = %d\n"
           "Input (matrices A and B) in %s, output (matrix C=A*B) in %s\n", m, n, k, in.c_str(), out.c_str());
    Matrix A(m, n);
    Matrix B(n, k);
    std::cout << "Starting..." << std::endl;
    A.generate();
    B.generate();
    Matrix C = A * B;
    std::cout << "...almost finished" << std::endl;
    A.fprint(in);
    B.fprint(in, std::ios_base::app);
    C.fprint(out);
    std::cout << "...finished" << std::endl;
    return EXIT_SUCCESS;
}
