N=4
PASS=1
for (( i=1; i<=$N; i++ )) do 
    ./a.out <tests/in${i}.txt >tests/out.txt
    diff -B --strip-trailing-cr tests/out${i}.txt tests/out.txt &>/dev/null
    ret_val=$?
    if [ $ret_val -ne 0 ]; then 
        echo "Test ${i}/${N}: FAILED"
		PASS=0
    else 
        echo "Test ${i}/${N}: PASSED"
    fi
done
if [ $PASS -eq 0 ]; then
	echo "1 or more tests are not passed, please fix and try again."
else
	echo "All tests are passed successfully :-)"
fi

