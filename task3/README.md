**Treasure Hunt**

Script to compile program:

`./compile.sh`

Script to run program:

`./run.sh`

Script to test program:

`./test.sh`
