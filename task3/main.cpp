#include <iostream>
#include <vector>
#include <limits>

class Point {
public:
    double x;
    double y;
public:
    explicit Point(int x = 0, int y = 0) : x(x), y(y) {}

    /**
     * Overload of an input operator for the convenience
     */
    friend std::istream &operator>>(std::istream &in, Point &p) {
        in >> p.x >> p.y;
        return in;
    }
};

class Segment {
public:
    Point start;
    Point end;
public:
    Segment() = default;

    Segment(const Point &p1, const Point &p2) : start(p1), end(p2) {}

    /**
     * Overload of an input operator for the convenience
     */
    friend std::istream &operator>>(std::istream &in, Segment &e) {
        in >> e.start >> e.end;
        return in;
    }

    /**
     * Checks if two edges intersect
     */
    bool intersects(const Segment &segment) {
        double dx1 = end.x - start.x;
        double dy1 = end.y - start.y;
        double dx2 = segment.end.x - segment.start.x;
        double dy2 = segment.end.y - segment.start.y;
        double dx3 = segment.start.x - start.x;
        double dy3 = segment.start.y - start.y;
        double det = dy1 * dx2 - dx1 * dy2;
        double s = (dx2 * dy3 - dy2 * dx3) / det;
        double t = (dx1 * dy3 - dy1 * dx3) / det;
        return !(s < 0 || s > 1 || t < 0 || t > 1);
    }
};

/**
 * Counts how many doors are required to be made to reach treasure
 */
unsigned countDoors(const std::vector<Segment> &walls, const Point &treasure, const Point &endpoint) {
    Segment straight_path(treasure, endpoint);
    unsigned doors_count = 0;
    for (auto &wall:walls) {
        doors_count += straight_path.intersects(wall);
    }
    return doors_count;
}

int main() {
    unsigned walls_count;
    std::cin >> walls_count;

    std::vector<Segment> walls(walls_count);
    for (auto &w:walls) {
        std::cin >> w;
    }

    Point treasure;
    std::cin >> treasure;

    unsigned min_doors_count = std::numeric_limits<unsigned int>::max();
    if (walls.empty()) {
        min_doors_count = 1;
    }
    for (auto &w:walls) {
        min_doors_count = std::min(min_doors_count, countDoors(walls, treasure, w.start));
        min_doors_count = std::min(min_doors_count, countDoors(walls, treasure, w.end));
    }
    std::cout << "Number of doors = " << min_doors_count << std::endl;
    return EXIT_SUCCESS;
}
