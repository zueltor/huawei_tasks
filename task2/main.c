#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define BUFF_SIZE 256
#define X 15
#define Y 10
#define EMPTY -1
#define NOT_SET -1
#define NOT_VISITED 0
#define BIG_CLUSTERS_EXIST 1
#define BONUS_POINTS 1000
#define BALLS_THRESHOLD 2

/**
 * Structure that represents balls cluster
 *
 * (x,y) - Column and row id of a chosen ball
 *
 * id - cluster id
 *
 * count - number of balls in a cluster
 *
 * color - color of balls in a cluster
 */
struct Cluster_t {
    int x;
    int y;
    int count;
    int id;
    char color;
};

struct Game_t {
    char board[Y][X];
    int balls_remaining;
    int score;
};

typedef struct Cluster_t Cluster;
typedef struct Game_t Game;

/**
 * Resets cluster_ids so that balls can be visited
 */
void reset(int cluster_ids[Y][X], char board[Y][X]) {
    for (int y = 0; y < Y; y++) {
        for (int x = 0; x < X; x++) {
            if (board[y][x] == EMPTY) {
                cluster_ids[y][x] = NOT_SET;
            } else {
                cluster_ids[y][x] = NOT_VISITED;
            }
        }
    }
}

/**
 * Recursively visits all not visited balls that belong to this cluster
 * @return number of visited balls (number of balls in a cluster)
 */
int visitBallsCluster(int current_x, int current_y, int color, int id, char board[Y][X],
                      int cluster_ids[Y][X], int *cluster_x, int *cluster_y) {
    if (current_x < 0 || current_x >= X || current_y < 0 || current_y >= Y || board[current_y][current_x] != color ||
        cluster_ids[current_y][current_x] != NOT_VISITED) {
        return 0;
    }

    cluster_ids[current_y][current_x] = id;
    if (current_x < *cluster_x ||
        (current_x == *cluster_x && current_y < *cluster_y)) {
        *cluster_x = current_x;
        *cluster_y = current_y;
    }

    int deltas[4][2] = {{1,  0},
                        {-1, 0},
                        {0,  1},
                        {0,  -1}};
    int balls_visited = 1;

    for (int i = 0; i < 4; i++) {
        balls_visited += visitBallsCluster(current_x + deltas[i][0], current_y + deltas[i][1], color, id,
                                           board, cluster_ids, cluster_x, cluster_y);
    }
    return balls_visited;
}

void swap(Cluster **c1, Cluster **c2) {
    Cluster *tmp = *c1;
    *c1 = *c2;
    *c2 = tmp;
}

/**
 * Visits all balls to find the biggest cluster
 * @param[out] max_cluster - biggest cluster
 */
void visitBalls(Cluster **max_cluster, char board[Y][X], int cluster_ids[Y][X]) {
    int id = 1;
    Cluster *cluster = (Cluster *) malloc(sizeof(Cluster));
    (*max_cluster)->count = 0;
    for (int y = 0; y < Y; y++) {
        for (int x = 0; x < X; x++) {
            if (cluster_ids[y][x] == NOT_VISITED) {
                cluster->color = board[y][x];
                int cluster_x = x;
                int cluster_y = y;
                cluster->count = visitBallsCluster(x, y, board[y][x], id, board, cluster_ids,
                                                   &cluster_x, &cluster_y);
                cluster->x = cluster_x;
                cluster->y = cluster_y;
                cluster->id = id;
                id++;
                if (cluster->count > (*max_cluster)->count ||
                    (cluster->count == (*max_cluster)->count &&
                     (cluster->x < (*max_cluster)->x))) {
                    swap(&cluster, max_cluster);
                }
            }
        }
    }
    free(cluster);
}

/**
 * Shifts the remaining balls in each column down to fill the empty spaces. The order of the balls in each column is preserved
 */
void shiftDown(char board[Y][X], int cluster_ids[Y][X], int id) {
    for (int y = 0; y < Y; y++) {
        for (int x = 0; x < X; x++) {
            while (cluster_ids[y][x] == id) {
                int yy;
                for (yy = y; yy < Y - 1; yy++) {
                    cluster_ids[yy][x] = cluster_ids[yy + 1][x];
                    board[yy][x] = board[yy + 1][x];
                }
                cluster_ids[yy][x] = NOT_SET;
                board[yy][x] = EMPTY;
            }
        }
    }
}

/**
 * If a column is empty, shifts the remaining columns to the left as far as possible. The order of the columns is preserved.
 */
void shiftLeft(char board[Y][X], int cluster_ids[Y][X]) {
    int empty_column;
    int max_X = X;
    for (int x = 0; x < max_X; x++) {
        empty_column = true;
        do {
            for (int y = 0; y < Y; y++) {
                if (cluster_ids[y][x] != NOT_SET) {
                    empty_column = false;
                    break;
                }
            }
            if (empty_column) {
                int xx;
                for (int y = 0; y < Y; y++) {
                    for (xx = x; xx < max_X - 1; xx++) {
                        cluster_ids[y][xx] = cluster_ids[y][xx + 1];
                        board[y][xx] = board[y][xx + 1];
                    }
                    cluster_ids[y][xx] = NOT_SET;
                    board[y][xx] = EMPTY;
                }
                max_X--;
            }
        } while (empty_column && x < max_X);
    }
}

/**
 * Prints current board
 */
void printBoard(char board[Y][X]) {
    printf("----\n");
    for (int y = Y - 1; y >= 0; y--) {
        for (int x = 0; x < X; x++) {
            printf("%c", board[y][x]);
        }
        printf("\n");
    }
}

/**
 * Reads board from stdin for each game
 */
int readGames(Game *games, int games_count) {
    char buffer[BUFF_SIZE];
    char *error;
    int x;
    int y;
    for (int g = 0; g < games_count; g++) {
        y = Y - 1;
        while (y >= 0) {
            error = fgets(buffer, BUFF_SIZE, stdin);
            if (error == NULL) {
                fprintf(stderr, "Failed to read line\n");
                return EXIT_FAILURE;
            }
            if (strlen(buffer) < X) {
                continue;
            }
            x = 0;
            while (x < X) {
                games[g].board[y][x] = buffer[x];
                x++;
            }
            y--;
        }
    }
    return EXIT_SUCCESS;
}

/**
 * "Plays" the game and prints output and prints the game number, followed by a new line, followed by information about each move,
 * followed by the final score, followed by a blank line.
 */
void processGame(Game *game, int game_number, int cluster_ids[Y][X]) {
    Cluster *max_cluster = (Cluster *) malloc(sizeof(Cluster));
    int points;
    int move_number = 1;
    game->score = 0;
    game->balls_remaining = X * Y;
    printf("Game %d:\n", game_number + 1);
    while (BIG_CLUSTERS_EXIST) {
        reset(cluster_ids, game->board);
        visitBalls(&max_cluster, game->board, cluster_ids);
        if (max_cluster->count < BALLS_THRESHOLD) {
            break;
        }
        points = (max_cluster->count - BALLS_THRESHOLD) * (max_cluster->count - BALLS_THRESHOLD);
        printf("Move %d at (%d,%d): removed %d balls of color %c, got %d points.\n",
               move_number++, max_cluster->y + 1, max_cluster->x + 1, max_cluster->count, max_cluster->color,
               points);
        game->score += points;
        game->balls_remaining -= max_cluster->count;
        shiftDown(game->board, cluster_ids, max_cluster->id);
        shiftLeft(game->board, cluster_ids);
    }
    if (game->balls_remaining == 0) {
        game->score += BONUS_POINTS;
    }
    free(max_cluster);
    printf("Final score: %d, with %d balls remaining.\n\n", game->score, game->balls_remaining);
}

int main() {
    int cluster_ids[Y][X];
    int games_count;
    int error;

    scanf("%d\n", &games_count);
    Game games[games_count];
    error = readGames(games, games_count);
    if (error) {
        return EXIT_FAILURE;
    }

    for (int g = 0; g < games_count; g++) {
        processGame(&games[g], g, cluster_ids);
    }
    return EXIT_SUCCESS;
}

