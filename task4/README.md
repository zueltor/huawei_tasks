**Digital Lab** 

Two dimensional pattern matching using [this algorithm](https://www.researchgate.net/publication/220421645_A_Technique_for_Two-Dimensional_Pattern_Matching)

Script to compile program:

`./compile.sh`

Script to run program:

`./run.sh`

Script to test program:

`./test.sh`
