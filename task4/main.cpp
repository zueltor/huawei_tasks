#include <iostream>
#include <vector>
#include <bitset>

#define PRIME 33554393

class Matrix {
    int rows;
    int columns;
    std::vector<std::vector<char> > a;
    std::vector<int> hash;
public:
    Matrix(int n1, int n2) : rows(n1), columns(n2), a(n1, std::vector<char>(n2)), hash(n2) {}

    Matrix(const Matrix &m) : rows(m.rows), columns(m.columns), a(m.a) {}

    void read(std::istream &in) {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                in >> a[i][j];
            }
        }
    }

    void print(std::ostream &out) {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                out << a[i][j] << " ";
            }
            out << std::endl;
        }
    }

    void computeHash(int n) {
        int d = 2;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < columns; j++) {
                hash[j] = (hash[j] * d + (a[i][j] - '0')) % PRIME;
            }
        }
    }

    std::vector<int> getPrefixFunction() {
        std::vector<int> prefix_func(columns);
        int j = 0;
        int i = 1;
        while (i < columns) {
            if (hash[i] == hash[j]) {
                prefix_func[i] = j + 1;
                i++;
                j++;
            } else if (j == 0) {
                prefix_func[i] = 0;
                i++;
            } else {
                j = prefix_func[j - 1];
            }
        }
        return prefix_func;
    }

    /**
     * Used when hashes are equal, compares each element to confirm the pattern is found
     */
    bool subMatrixFound(int row, int column, Matrix &submatrix) {
        for (int i = 0; i < submatrix.rows; i++) {
            for (int j = 0; j < submatrix.columns; j++) {
                if (a[i + row][j + column] != submatrix.a[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    void markFoundMatrix(int row, int column, const Matrix &submatrix) {
        for (int i = 0; i < submatrix.rows; i++) {
            for (int j = 0; j < submatrix.columns; j++) {
                char c = a[i + row][j + column];
                if (c == '0') {
                    c = '*';
                } else if (c == '1') {
                    c = '2';
                }
                a[i + row][j + column] = c;
            }
        }
    }

    void recomputeHash(int current_row, int dn, int pattern_rows) {
        for (int j = 0; j < columns; j++) {
            hash[j] = (hash[j] - (a[current_row][j] - '0') * dn) * 2 + a[current_row + pattern_rows][j] - '0';
        }
    }

    /**
     * Search pattern using Rabin-Karp algorithm vertically, and then Knuth-Morris-Pratt algorithm row by row
     */
    Matrix searchPatternRKKMP(Matrix &pattern) {
        int current_row = 0;
        Matrix result = *this;
        pattern.computeHash(pattern.rows);
        computeHash(pattern.rows);

        //dn is needed to recompute hash
        int dn = 1;
        for (int i = 1; i < pattern.columns; i++) {
            dn = (2 * dn) % PRIME;
        }

        std::vector<int> prefix_func = pattern.getPrefixFunction();
        int max_row = rows - pattern.rows;

        while (current_row <= max_row) {
            searchPatternKMP(pattern, current_row, result, prefix_func);
            if (current_row < max_row) {
                recomputeHash(current_row, dn, pattern.rows);
            }
            current_row++;
        }
        return result;
    }

    void searchPatternKMP(Matrix &pattern, int current_row, Matrix &result, const std::vector<int> &prefix_func) {
        int pattern_length = pattern.hash.size();
        int k = 0;
        int l = 0;
        while (k < columns) {
            if (hash[k] == pattern.hash[l]) {
                k++;
                l++;
                if (l == pattern_length) {
                    int current_column = k - l;
                    if (subMatrixFound(current_row, current_column, pattern)) {
                        result.markFoundMatrix(current_row, current_column, pattern);
                        k = k - l + 1;
                        l = 0;
                    }
                }
            } else {
                if (l == 0) {
                    k++;
                } else {
                    l = prefix_func[l - 1];
                }
            }
        }
    }
};

int main() {
    int p1, p2;
    std::cin >> p1 >> p2;
    Matrix pattern(p1, p2);
    pattern.read(std::cin);

    int n1, n2;
    std::cin >> n1 >> n2;
    Matrix text(n1, n2);

    text.read(std::cin);

    Matrix result = text.searchPatternRKKMP(pattern);
    result.print(std::cout);
    return EXIT_SUCCESS;
}
